<?php
/**
 * Displays footer site info
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<div class="site-info">
	<?php

	// echo get_theme_mod('site_navigation_bg');

	if ( function_exists( 'the_privacy_policy_link' ) ) {
		the_privacy_policy_link( '', '<span role="separator" aria-hidden="true"></span>' );
	}
	?>
	<a href="<?php echo esc_url( __( 'https://www.simplonlyon.fr/', 'twentyseventeen' ) ); ?>" class="imprint">
		<?php
		
	echo get_theme_mod('para_color');
		printf( __( 'Proudly powered by %s', 'twentyseventeen' ), 'Simplon' ); ?>
	</a>
</div><!-- .site-info -->
