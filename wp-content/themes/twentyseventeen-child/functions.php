<?php

add_action('wp_enqueue_scripts', 'parent_style');


function parent_style() {
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');

}

add_action('customize_register', 'customizer_enhanced');

function customizer_enhanced ( WP_Customize_Manager $wP_Customize_Manager){
    $wP_Customize_Manager->add_section('simplon_section', [
        'title' =>'Simplon Option',
        'priority' => 30
    ]);

    $wP_Customize_Manager->add_setting('site_navigation_bg', [
        'default' => '#FFF',
        'transport' => 'refresh'
    ]);

    $wP_Customize_Manager->add_control(new WP_Customize_Color_Control($wP_Customize_Manager, 'nav_bg', [
        'label' => 'Navigation Background',
        'section' => 'simplon_section',
        'settings' => 'site_navigation_bg'
    ]));
}

add_action('wp_head', 'customizer_style');

function customize_style() {
    ?>
    <style>
        #site-navigation {
            background-color: <?php echo get_theme_mod('site_navigation_bg') ?>;
        }

    </style>
    <?php
}