# Entour'âge Solidaire

## Contact

- *Président* : Philippe Albanel

- *Réseau* : Anciela

- *Mail* : pauline@entourage-solidaire.fr



## Projet

​	Le projet Chez Daddy est un café intergénérationnel qui ouvrira ses portes en juin 2020 à la Croix-Rousse.

​	Le café proposera aux habitants un lieu convivial de proximité avec un service de boissons et de petites restaurations, la présence de nombreux jeux de sociétés et un espace adapté aux enfants. De nombreuses activités et animations y seront proposées à partir des initiatives des habitants et des partenaires locaux. L’objectif étant de favoriser la transmission de savoir intergénérationnelle et de faire découvrir les actions menées par les partenaires locaux.

**Fonctionnalités :** Les besoins au niveau du site seraient une présentation du café, la programmation des activités et la possibilité de s’y inscrire en ligne en l’intégrant dans le site que nous avons déjà actuellement www.entourage-solidaire.fr .

=> Site de présentation / Agenda + Inscription / Actualité



## Mail Lundi 4/10 :

Madame, Monsieur,

Tout d'abord merci d'avoir choisi Simplon pour vous aider à la création de votre projet. Nous, Sophie et Florent, sommes très motivés à l'idée de vous aider dans le développement de votre association.

Afin d'avancer rapidement sur ce projet, seriez vous disponible pour établir un premier contact et définir ensemble les fonctionnalités, le design et les interactions utilisateur que vous souhaitez intégrer dans votre site ?

De notre côté, nous démarrons dès cet après midi à travailler sur votre projet. Ainsi, nous serons en mesure de vous fournir les premières maquetttes dès la fin de journée. Vous pourrez alors choisir vos préférences et nous indiquer également vos exigences.

Nous sommes évidemment à votre disposition pour toute information complémentaire. Pour nous joindre : soph.brunier@gmail.com ou florent.gallitre@gmail.com.

Cordialement, 



Sophie et Florent.